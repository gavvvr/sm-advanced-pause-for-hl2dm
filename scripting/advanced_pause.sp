#pragma semicolon 1

#include <sourcemod>
#include <steamtools>
#include <smlib>
#include <morecolors>
#include <updater>

#define UPDATE_URL "http://bitbucket.org/gavvvr/sm-advanced-pause-for-hl2dm/raw/master/advanced-pause.txt"

#define PLUGIN_VERSION      "0.9.4"
#define SOUND_BLIP          "buttons/blip1.wav"
#define SOUND_GO            "hl1/fvox/beep.wav"
#define SOUND_ERROR         "buttons/button10.wav"
#define PAUSE_MSGPREFIX     "{orangered}||"

new unpauseDelay;
new bool:onpause = false;
new bool:unpause_delayed = false;
new bool:pauseCmdDelaying = false;

new Handle:g_Cvar_Pausable = INVALID_HANDLE;
new Handle:g_Cvar_SpecsCanPause = INVALID_HANDLE;
new Handle:g_Cvar_LetInOnPause = INVALID_HANDLE;
new Handle:g_Cvar_PauseCmdDelay = INVALID_HANDLE;
new Handle:g_Cvar_CountDnSeconds = INVALID_HANDLE;
new Handle:g_Cvar_PausableAfterGameEnd = INVALID_HANDLE;

public Plugin:myinfo =
{
	name = "Advanced hl2dm pause",
	author = "hl2dm.net",
	description = "Advanced pause for competitive gameplay with chat fix",
	version = PLUGIN_VERSION,
	url = "http://hl2dm.net/"
};

public OnPluginStart()
{
	AddCommandListener(Command_Pause, "pause");
	g_Cvar_Pausable = FindConVar("sv_pausable");

	g_Cvar_SpecsCanPause = CreateConVar("sm_specpause", "0", "Determines if spectators can use pause or not", _, true, 0.0, true, 1.0);
	g_Cvar_LetInOnPause = CreateConVar("sm_letinonpause", "1", "Determines if players can join on paused server. Such clients will quickly toggle pause so they won't stuck on loading screen", _, true, 0.0, true, 1.0);
	g_Cvar_PauseCmdDelay = CreateConVar("sm_afterpausecooldown", "2", "Sets the delay being used to ignore pause commands from clients righ after the game was paused.", _, true, 0.1, false, _);
	g_Cvar_CountDnSeconds = CreateConVar("sm_countdownseconds", "3", "The amount of time in seconds used by countdown. Setting this to 0 disables countdown.", _, true, 0.0, false, _);
	g_Cvar_PausableAfterGameEnd = CreateConVar("sm_pauseaftergameend", "0", "Determines if pause command is availible after the game ended", _, true, 0.0, true, 1.0);

	AutoExecConfig();

	if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
}

public OnConfigsExecuted()
{
	unpauseDelay = GetConVarInt(g_Cvar_CountDnSeconds) -1;

	if (!GetConVarBool(g_Cvar_PausableAfterGameEnd))
	{
		HookUserMessage(GetUserMessageId("VGUIMenu"), Hook_VGUIMenu);
	}
}

public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
}

public Action:OnClientSayCommand(client, const String:command[], const String:sArgs[])
{
	if (!onpause)
	{
		return Plugin_Continue;
	}
	new String:messageArg[MAX_MESSAGE_LENGTH];
	strcopy(messageArg, sizeof(messageArg), sArgs);
	StripQuotes(messageArg);
	new clientTeam;
	clientTeam = GetClientTeam(client);
	if (StrEqual(command, "say_team", false))
	{
		for (new i = 1; i <= MaxClients; i++)
		{
			if(!IsClientInGame(i))
			{
				continue;
			}
			else if (clientTeam == GetClientTeam(i))
			{
				if (clientTeam == 1) //spectator
				{
					CPrintToChatEx(i, client,"%s {default}(Spectator) {teamcolor}%N{default} : %s", PAUSE_MSGPREFIX, client, messageArg);
				}
				else
				{
					CPrintToChatEx(i, client,"%s {default}(TEAM) {teamcolor}%N{default} : %s", PAUSE_MSGPREFIX, client, messageArg);
				}
			}
		}
		return Plugin_Handled;
	}
	else if (StrEqual(command, "say", false))
	{
		if (clientTeam == 1) //spectator
		{
			CPrintToChatAllEx(client,"%s {default}*SPEC* {teamcolor}%N{default} : %s", PAUSE_MSGPREFIX, client, messageArg);
		}
		else
		{
			CPrintToChatAllEx(client,"%s {teamcolor}%N{default} : %s", PAUSE_MSGPREFIX, client, messageArg);
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:Hook_VGUIMenu(UserMsg:msg_id, Handle:msg, const players[], playersNum, bool:reliable, bool:init)
{
	new String:s[10];
	BfReadString(msg, s, sizeof(s));
	if (strcmp(s, "scores", false) == 0)
	{
		pauseCmdDelaying = true;
		return Plugin_Continue;
	}
	return Plugin_Continue;
}

public OnMapEnd()
{
	onpause = false;
	unpause_delayed = false;
	pauseCmdDelaying = false;
}

public OnClientDisconnect_Post(client)
{
	if(Client_GetCount(true, false) == 0)
	{
		onpause = false;
		unpause_delayed = false;
		pauseCmdDelaying = false;
	}

}

public OnClientPutInServer(client)
{
	if(!onpause || !GetConVarBool(g_Cvar_LetInOnPause))
	{

	}
	else
	{
		unpause_delayed = true;
		FakeClientCommand(client, "pause");
		CreateTimer(0.1, PauseAgainIn01sec, GetClientUserId(client));
	}
}

public Action:PauseAgainIn01sec(Handle:timer, any:usrID)
{
	new client = GetClientOfUserId(usrID);
	if (client == 0)
	{
		return Plugin_Continue;
	}
	FakeClientCommand(client, "pause");
	return Plugin_Continue;
}


public Action:Command_Pause(client, const String:command[], args)
{
	if(!GetConVarBool(g_Cvar_Pausable) || pauseCmdDelaying || (GetClientTeam(client) == 1 && !GetConVarBool(g_Cvar_SpecsCanPause)))
	{
		return Plugin_Handled;
	}
	if (!onpause)
	{
		CPrintToChatAll("{aliceblue}[SM] {purple}%N{aliceblue} paused the game", client);
		onpause = true;
		unpause_delayed = false;
		pauseCmdDelaying = true;
		CreateTimer(GetConVarFloat(g_Cvar_PauseCmdDelay), StopDelayingPauseCmd);
		return Plugin_Continue;
	}
	else if (!unpause_delayed)
	{
		if (unpauseDelay<0)
		{
			onpause = false;
			return Plugin_Continue;
		}
		PrintCenterTextAll("%i", GetConVarInt(g_Cvar_CountDnSeconds));
		PlaySoundAll(SOUND_BLIP);
		CreateTimer(1.0, Unpause_Countdown, GetClientUserId(client), TIMER_REPEAT);
		CPrintToChatAll("{aliceblue}[SM] The game will be unpaused in %i seconds by {purple}%N", GetConVarInt(g_Cvar_CountDnSeconds), client);
		pauseCmdDelaying = true;
		return Plugin_Handled;
	}
	onpause = false;
	unpause_delayed = false;
	return Plugin_Continue;
}

public Action:Unpause_Countdown(Handle:timer, any:usrID)
{
	if (unpauseDelay == 0)
	{
		new client = GetClientOfUserId(usrID);
		PrintCenterTextAll("");
		if (IsClientInGame(client))
		{
			PlaySoundAll(SOUND_GO);
			unpauseDelay = GetConVarInt(g_Cvar_CountDnSeconds)-1;
			pauseCmdDelaying = false;
			unpause_delayed = true;
			FakeClientCommand(client, "pause");
			return Plugin_Stop;
		}
		CPrintToChatAll("{aliceblue}[SM] Failed to unpause the game, because the \"unpauser\" left the game");
		PlaySoundAll(SOUND_ERROR);
		unpauseDelay = GetConVarInt(g_Cvar_CountDnSeconds)-1;
		pauseCmdDelaying = false;
		return Plugin_Stop;
	}
	PrintCenterTextAll("%i", unpauseDelay);
	PlaySoundAll(SOUND_BLIP);
	unpauseDelay--;
	return Plugin_Continue;
}

public Action:StopDelayingPauseCmd(Handle:timer)
{
	pauseCmdDelaying = false;
}

PlaySoundAll(String:sndpath[])
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(!IsClientInGame(i) || IsFakeClient(i))
		{
			continue;
		}
		ClientCommand(i, "playgamesound \"%s\"", sndpath);
	}
}